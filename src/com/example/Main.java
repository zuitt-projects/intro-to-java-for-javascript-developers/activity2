package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("** Activity 2 **");

        Scanner myInput = new Scanner(System.in);
        System.out.print("Enter Year: ");
        int year = myInput.nextInt();

        if(year % 4 == 0) {
            if(year % 100 == 0) {
                if(year % 400 == 0) {
                    System.out.println(year + " is leap year;");
                } else {
                    System.out.println(year + " is not leap year;");
                }
            } else {
                System.out.println(year + " is leap year;");
            }

        }
    }
}
